"use strict"
// debugger
const buttonsElements = document.querySelector('.btn-wrapper');
let dustbin = document.querySelector('.btn');

document.addEventListener('keydown', (e) => {
    if(document.querySelector(`[data-key = "${e.key}"]`)) {
        if(dustbin) {
            dustbin.classList.remove('btnColor');
        }
        document.querySelector(`[data-key = "${e.key}"]`).classList.add('btnColor');
        dustbin = document.querySelector(`[data-key = "${e.key}"]`);
    }
})

buttonsElements.addEventListener('click', (e) => {
    if(e.target !== e.currentTarget && e.target.classList.contains('btn')) {
        e.target.classList.add('btnColor');
        if(dustbin) {
            dustbin.classList.remove('btnColor');
        }
    }
    dustbin = e.target;
})